$(document).ready(function() {

  //
  /*$('.vid-bg').vide({
    mp4:'video/video1.mp4',
    ogv:'video/video1.ogv',
    webm:'video/video1.webm',  
    poster:'img/bg1.jpg'
  }, {
    playbackRate: 1,
    muted: true,
    loop: true,
    autoplay: true,
    position: '50% 50%', 
    posterType: 'jpg',
    resizing: true, 
    bgColor: '#091232'
  });*/

	//placeholder
	$('[placeholder]').focus(function() {
		var input = $(this);
		if(input.val() == input.attr('placeholder')) {
			input.val('');
			input.removeClass('placeholder');
		}
	}).blur(function() {
		var input = $(this);
		if(input.val() == '' || input.val() == input.attr('placeholder')) {
			input.addClass('placeholder');
			input.val(input.attr('placeholder'));
		}
	}).blur();
	$('[placeholder]').parents('form').submit(function() {
		$(this).find('[placeholder]').each(function() {
			var input = $(this);
			if(input.val() == input.attr('placeholder')) {
				input.val('');
			}
		})
	});

  //
  $('.mobile-button').click(function() {
      $(this).toggleClass('open');
      $('.header').toggleClass('open');
      $('body').toggleClass('open');
      return false;
  });

  //fixed header
  function showDiv() {
    if ($(window).scrollTop() > 0 && $('.header').data('positioned') == 'false') {
      $(".header").data('positioned', 'true');
      $(".header").addClass('fix');
    }else if ($(window).scrollTop() <= 0 && $('.header').data('positioned') == 'true') {
      $(".header").fadeIn(0, function() {
        $(".header").removeClass('fix');
      }).data('positioned', 'false');
    }
  }
  //$(window).scroll(showDiv);
  $('.header').data('positioned', 'false');

  $('.carou').owlCarousel({
    margin:0,
    loop:true,
    autoWidth:true,
    items:4
  });

  //
  var $preloader = $('#page-preloader'),
        $spinner   = $preloader.find('.spinner');
        $spinner.fadeOut();
        $preloader.delay(10).fadeOut('slow');

   //
  var ww = document.body.clientWidth;
  var wh = $('body').height();
  $(document).ready(function() {
    adjustMenu();
  });
  $(window).bind('resize orientationchange', function() {
      ww = document.body.clientWidth;
      wh = $('body').height();
      adjustMenu();
  });

  var adjustMenu = function() {
      if(ww < 1024) {                       
        $('.acc__drop.active').slideDown(300);
        $('.acc__opener').click(function() {          
            $(this).parent().siblings('.acc__item').find('.acc__drop').removeClass('active');
            $(this).parent().siblings('.acc__item').find('.acc__opener').removeClass('active');
            $(this).parent().siblings('.acc__item').find('.acc__drop').slideUp(300);
            $(this).next('.acc__drop').addClass('active');
            $(this).next('.acc__drop').slideDown(300);
            $(this).addClass('active');
            return false;
        });
        $('.animated').appear(function() {
          var element = $(this);
          var animation = element.data('animation');
          var animationDelay = 0;
          if (animationDelay) {
            setTimeout(function(){
              element.addClass( animation + " visible" );
              element.removeClass('hiding');

            }, animationDelay);
          }else {
            element.addClass( animation + " visible" );
            element.removeClass('hiding');

          }    
        },{accY: -50});
      }
      else if(ww >= 1024) {         
        $('.acc__opener').click(function() {          
            $(this).parent().siblings('.acc__item').find('.acc__drop').removeClass('active');
            $(this).parent().siblings('.acc__item').find('.acc__opener').removeClass('active');
            $(this).parent().siblings('.acc__item').find('.acc__desc').slideUp(300);
            $(this).next('.acc__drop').addClass('active');
            $(this).next('.acc__drop').find('.acc__desc').slideDown(300);
            $(this).addClass('active');
            return false;
        });
        
      }
  }

  if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
    $('html').addClass('touch');
  }
  else{
    $('html').addClass('web');
  }

  //animation on page scroll
  $('.animated').appear(function() {
      var element = $(this);
      var animation = element.data('animation');
      var animationDelay = element.data('delay');
      if (animationDelay) {
        setTimeout(function(){
          element.addClass( animation + " visible" );
          element.removeClass('hiding');

        }, animationDelay);
      }else {
        element.addClass( animation + " visible" );
        element.removeClass('hiding');

      }    
  },{accY: -50});

  //
  $('.counter').each(function() {
    var $this = $(this),
        countTo = $this.attr('data-count');    
    $({ countNum: $this.text()}).animate({
      countNum: countTo
    },
    {
      duration: 1000,
      easing:'linear',
      step: function() {
        $this.text(Math.floor(this.countNum));
      },
      complete: function() {
        $this.text(this.countNum);
      }
    });  
  });

  //
  $(".scroll__link").click(function() {
    $("html, body").animate({
        scrollTop: $('#scroll__target').offset().top - 50
    }, 700)
  });
  $(".btn-local, a[href='#scontact']").click(function() {
    $("html, body").animate({
        scrollTop: $('#scontact').offset().top - 50
    }, 700)
  });

  document.addEventListener("touchstart", function() {},false);

});


$(window).mousemove(function(e) {
    var ypos=e.clientY;
    var ypos=ypos*5;

    $('.web .vert1').css('margin-top',(- (0+(ypos/50))+"px"));
    $('.web .vert2').css('margin-top',((0+(ypos/50))+"px"));

    $('.web .hor').css('transform', 'rotate(0deg)');
    var rotate_Y;
    var invert = false;
    
    if (invert) {
      rotate_Y = e.pageY*0.11;
    } else if (!invert) {
      rotate_Y = -e.pageY*0.11;
    }
    
    $('.web .hor1, .web .hor4').css('transform', 'rotate(' + rotate_Y + 'deg)')
    $('.web .hor2, .web .hor3').css('transform', 'rotate(' + (- rotate_Y) + 'deg)')
                   
});


function create_message() {
     if ($("#message_name").pVal()
     && $("#message_email").pVal()
     && $("#message_content").pVal()) 
  {
    
        $.post("https://mainapi.phenom.team/internalMessages/create", { 'name': $("#message_name").pVal(),
                                'email': $("#message_email").pVal(),
                                'message': $("#message_content").pVal()})
            .done(function (data) {
                $("#message_form").hide();
                $("#message_sent").show();
            })
            .fail(function (xhr, status, error) {
                alert(xhr.responseJSON.message);
            });

     }
    return false;

}
$(window).load(function() {

  


});


$.fn.pVal = function(){
    var $this = $(this),
        val = $this.eq(0).val();
    if(val == $this.attr('placeholder'))
        return '';
    else
        return val;
}
